name := "Game of Life"

organization := "com.lucaongaro"

version := "0.0.1"

scalaVersion := "2.10.3"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.0" % "test"

initialCommands := "import com.lucaongaro.gameoflife._"

