package com.lucaongaro.gameoflife

trait Cell {
  val alive: Boolean
  def evolve( aliveNeighbors: Int ): Cell
  override def toString = if ( alive ) "*" else " "
}

class AliveCell extends Cell {
  val alive = true

  def evolve( aliveNeighbors: Int ) = {
    if ( aliveNeighbors < 2 || aliveNeighbors > 3 ) new DeadCell()
    else this
  }
}

class DeadCell extends Cell {
  val alive = false

  def evolve( aliveNeighbors: Int ) = {
    if ( aliveNeighbors == 3 ) new AliveCell()
    else this
  }
}
