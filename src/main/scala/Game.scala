package com.lucaongaro.gameoflife

class Game(
  val width:   Int,
  val height:  Int,
  aliveCoords: Set[(Int, Int)]
) extends Iterator[World] {

  private var world = World( width, height, aliveCoords )

  def hasNext() = true

  def next() = {
    val w = world
    world = world.evolve
    w
  }
}
