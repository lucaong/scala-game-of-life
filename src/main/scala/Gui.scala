package com.lucaongaro.gameoflife

import java.awt._
import processing.core._

class Gui(
  game:   Game
) extends PApplet {

  override def setup() = {
    size( game.width * 10, game.height * 10 )
    fill( 80 )
    stroke( 80 )
    frameRate( 24 )
  }

  override def draw() = {
    background( 200 )
    val world = game.next()
    for (
      y <- 0 until height;
      x <- 0 until width
    ) {
      if ( world.cellAt( x, y ).alive ) {
        rect( x * 10, y * 10, 10, 10 )
      }
    }
  }
}

object Gui {
  def main( args: Array[String] ) {
    val ( width, height ) = ( args(0).toInt, args(1).toInt )
    val initial = Set( (3, 2), (5, 3), (2, 4), (3, 4), (6, 4), (7, 4), (8, 4) )
    val game    = new Game( width, height, initial )
    val gui     = new Gui( game )
    val frame   = new Frame("Game of Life")

    frame.setLayout( new BorderLayout() );
    frame.add( gui, BorderLayout.CENTER );
    frame.pack()
    frame.setSize( new Dimension( width * 10, height * 10 ) );
    frame.setTitle("Game of Life")
    frame.setVisible(true)

    gui.init()
  }
}
