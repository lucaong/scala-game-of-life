package com.lucaongaro.gameoflife

class World(
  width:  Int,
  height: Int,
  cells:  Vector[Cell]
) {
  require( cells.length == width * height,
    "Number of cells should equal width times height" )

  def evolve = {
    val newCells = cells.zipWithIndex.map { case (cell, idx) =>
      val ( x, y ) = ( idx % width, idx / width )
      cell.evolve( neighborsOf( x, y ).count( _.alive ) )
    }
    new World( width, height, newCells )
  }

  def neighborsOf( x: Int, y: Int ) = for {
    i <- -1 to 1
    j <- -1 to 1
    if !( i == 0 && j == 0 )
  } yield cellAt( x + i, y + j )

  def cellAt( x: Int, y: Int ) = {
    val nx = ( x % width + width ) % width
    val ny = ( y % height + height ) % height
    cells( width * ny + nx )
  }

  override def toString = {
    cells.grouped( width ).map( _.mkString ).mkString("\n")
  }
}

object World {
  def apply(
    width:       Int,
    height:      Int,
    aliveCoords: Set[(Int, Int)]
  ) = {
    val cells = for {
      y <- 0 until height
      x <- 0 until width
    } yield {
      if ( aliveCoords (x, y) ) new AliveCell()
      else new DeadCell()
    }
    new World( width, height, cells.toVector )
  }
}
