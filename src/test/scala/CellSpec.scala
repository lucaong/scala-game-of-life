package com.lucaongaro.gameoflife

import org.scalatest.FunSpec
import org.scalatest.Matchers

class CellSpec extends FunSpec with Matchers {
  describe("AliveCell") {
    it("is alive") {
      ( new AliveCell() ).alive should be ( true )
    }

    describe("evolve") {
      it("returns a dead cell if there are less than 2 alive neighbors") {
        val cell = new AliveCell()
        val evolved = cell.evolve( 1 )
        evolved.alive should be ( false )
      }

      it("returns a dead cell if there are more than 3 alive neighbors") {
        val cell = new AliveCell()
        val evolved = cell.evolve( 4 )
        evolved.alive should be ( false )
      }

      it("returns an alive cell if there are 2 alive neighbors") {
        val cell = new AliveCell()
        val evolved = cell.evolve( 2 )
        evolved.alive should be ( true )
      }

      it("returns an alive cell if there are 3 alive neighbors") {
        val cell = new AliveCell()
        val evolved = cell.evolve( 3 )
        evolved.alive should be ( true )
      }
    }
  }

  describe("DeadCell") {
    it("is not alive") {
      ( new DeadCell() ).alive should be ( false )
    }

    describe("evolve") {
      it("returns an alive cell if there are exactly 3 alive neighbors") {
        val cell = new DeadCell()
        val evolved = cell.evolve( 3 )
        evolved.alive should be ( true )
      }

      it("returns a dead cell if there are less than 3 alive neighbors") {
        val cell = new DeadCell()
        val evolved = cell.evolve( 2 )
        evolved.alive should be ( false )
      }

      it("returns an alive cell if there are more than 3 alive neighbors") {
        val cell = new DeadCell()
        val evolved = cell.evolve( 4 )
        evolved.alive should be ( false )
      }
    }
  }
}
