package com.lucaongaro.gameoflife

import org.scalatest.FunSpec
import org.scalatest.Matchers

class WorldSpec extends FunSpec with Matchers {
  describe("World") {
    describe("cellAt") {
      it("returns the cell at the given coordinates") {
        val w = World( 10, 10, Set( (3,5) ) )
        w.cellAt( 3, 5 ).alive should be ( true )
        w.cellAt( 0, 0 ).alive should be ( false )
      }
    }

    describe("neighborsOf") {
      it("returns the neighbors of the cell in the given coordinates") {
        val w = World( 10, 10, Set( (5,4), (4,4), (5, 5), (1,2) ) )
        w.neighborsOf( 5, 5 ).count( _.alive ) should be ( 2 )
      }
    }

    describe("evolve") {
      it("returns a new evolved World") {
        val w = World( 5, 5, Set( (2,1), (2,2), (2, 3) ) )
        w.evolve.toString should be(
          """     
            |     
            | *** 
            |     
            |     """.stripMargin
        )
      }
    }
  }
}

